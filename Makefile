# apito-aspnet is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# apito-aspnet is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with apito-aspnet. If not, see <https://www.gnu.org/licenses/>.

# DEPENDENCIES: podman, gawk, fzf, guix.

# LOAD ENV FILES
-include envs/.env.*

RUNNER ?= podman
POD_NAME := apito
NAME := apito-jakartaee
VERSION := $(shell gawk '/<version>/ { version=substr($$1,10,5); print version; exit }' pom.xml)
BACKEND_IMAGE=${GITLAB_REGISTRY}/${USER}/${NAME}:${VERSION}
BACKEND_FOLDER=/app

# ======================================= MAIN

.PHONY: up
up: initial database.postgres

.PHONY: down
down:
	${RUNNER} pod rm --force ${POD_NAME}
	${RUNNER} container rm --force ${DATABASE_NAME}
	${RUNNER} volume rm --force ${DATABASE_DATA}
	${RUNNER} container rm --force ${NAME}

.PHONY: start
start: server.payara.run

# =========================== POD

initial:
	${RUNNER} pod create \
		--publish ${SERVER_HTTP_PORT}:${PAYARA_INTERNAL_HTTP_PORT} \
		--publish ${SERVER_ADMIN_PORT}:${PAYARA_INTERNAL_ADMIN_PORT} \
		--publish ${FRONTEND_PORT}:${FRONTEND_INTERNAL_PORT} \
		--publish 5432:5432 \
		--name ${POD_NAME}

# =========================== DATABASE

.PHONY: database.postgres
database.postgres:
	# ----------- ADD DATABASE
	${RUNNER} rm -f ${DATABASE_NAME}
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME} \
		--name ${DATABASE_NAME} \
		--env POSTGRES_PASSWORD=${SQL_PASSWORD} \
		--env POSTGRES_USER=${SQL_USERNAME} \
		--env POSTGRES_DB=${SQL_DATABASE} \
		--volume ${DATABASE_DATA}:${SQL_DATA}:Z \
		${DATABASE_IMAGE}

.PHONY: database.postgres.repl
database.postgres.repl:
	# ----------- DATABASE REPL
	${RUNNER} exec \
		--interactive --tty \
		${DATABASE_NAME} \
		psql --username ${SQL_USERNAME} --dbname ${SQL_DATABASE}

.PHONY: database.postgres.test
database.postgres.test:
	# ----------- ADD DATABASE TESTING
	${RUNNER} rm --force ${DATABASE_NAME}-test
	${RUNNER} volume rm -f ${DATABASE_DATA}-test

	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME} \
		--name ${DATABASE_NAME}-test \
		--env POSTGRES_PASSWORD=${SQL_PASSWORD} \
		--env POSTGRES_USER=${SQL_USERNAME} \
		--env POSTGRES_DB=${SQL_DATABASE}_test \
		--volume ${DATABASE_DATA}-test:${SQL_DATA}:Z \
		${DATABASE_IMAGE}

# database.test.old:
# 	${RUNNER} exec -it ${DATABASE_NAME} \
# 		psql --username ${SQL_USERNAME} --dbname ${SQL_DATABASE} \
# 			--command 'drop database if exists ${SQL_DATABASE}_test'
# 	${RUNNER} exec -it ${DATABASE_NAME} \
# 		psql --username ${SQL_USERNAME} --dbname ${SQL_DATABASE} \
# 			--command 'create database ${SQL_DATABASE}_test'


# ================================= APPLICATION SERVERS

.PHONY: server.liberty.run
server.liberty.run:
	${RUNNER} container rm --force ${NAME}-server
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME}\
		--env-file ./envs/.env.db \
		--name ${NAME}-server \
		--volume ${PWD}/ops/liberty-server.xml:/config/server.xml:Z \
		--volume ${PWD}/target/${POD_NAME}.war:/config/dropins/${POD_NAME}.war:Z \
		${NAME}-appserver

.PHONY: server.wildfly.run
server.wildfly.run:
	${RUNNER} container rm --force ${NAME}-server
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME}\
		--env-file ./envs/.env.db \
		--name ${NAME}-server \
		--volume ${PWD}/target/${POD_NAME}.war:/opt/jboss/wildfly/standalone/deployments/${POD_NAME}.war:Z \
		${NAME}-appserver

.PHONY: server.payara.run
server.payara.run:
	${RUNNER} container rm --force ${NAME}-server
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME}\
		--env-file ./envs/.env.db \
		--name ${NAME}-server \
		--volume ${PWD}/target/${POD_NAME}.war:/opt/payara/deployments/${POD_NAME}.war:Z \
		${NAME}-appserver

.PHONY: server.payara.redeploy
server.payara.redeploy:
	${RUNNER} exec \
		--workdir /opt/payara \
			${NAME}-server \
				java -jar /opt/payara/payara-micro.jar \
				--user ${APP_SERVER_USER} \
				--passwordfile ./deployments/passwordfile \
				redeploy \
					--name ${POD_NAME} \
					./deployments/${POD_NAME}.war

.PHONY: server.payara.repl
server.payara.repl:
		${RUNNER} exec \
			--interactive --tty \
			${NAME}-server \
			sh

.PHONY: prod
prod:
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--detach \
		--name ${NAME} \
		${NAME}:${VERSION}

.PHONY: clean
clean:
	rm -rf ./target/* # to avoid occasional compilation issues

.PHONY: package
package:
	${RUNNER} container rm --force ${NAME}-start
	${RUNNER} run \
		--rm \
		--name ${NAME}-start \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		${BACKEND_IMAGE} \
		bash -c './mvnw clean package'

.PHONY: embedded
embedded:
	${RUNNER} container rm --force ${NAME}-start
	${RUNNER} run \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-embedded \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env-file ./envs/.env.db \
		${BACKEND_IMAGE} \
		bash -c './mvnw clean package cargo:run'

.PHONY: repl
repl:
	${RUNNER} container rm --force ${NAME}-repl
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --interactive --tty \
		--name ${NAME}-repl \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env-file ./envs/.env.security \
		--env-file ./envs/.env.db \
		--quiet \
		${OPENJDK_IMAGE} \
		bash

.PHONY: command
command:
	${RUNNER} container rm --force ${NAME}-command
	${RUNNER} run \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-command \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env-file ./envs/.env.db \
		--quiet \
		${OPENJDK_REPL_IMAGE} \
		bash -c './mvnw $(shell cat ./ops/container-commands | fzf)'

.PHONY: test.integration
test.integration:
	${RUNNER} container rm --force ${NAME}-test
	${RUNNER} run \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-test \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env-file ./envs/.env.db \
		${OPENJDK_IMAGE} \
		bash -c './mvnw test'

.PHONY: logs
logs:
	${RUNNER} logs --follow ${NAME}-server

.PHONY: stats
stats:
	${RUNNER} pod stats ${POD_NAME}

# ============================================= TASKS

.PHONY: image.build.server
image.build.server:
	${RUNNER} build \
		--file $(shell ls ./Dockerfile* | fzf) \
		--tag ${NAME}-appserver

.PHONY: image.build.backend
image.build.backend:
	# ---------------------- BUILD BACKEND IMAGE
	${RUNNER} build \
		--file ./Dockerfile \
		--tag ${BACKEND_IMAGE}

.PHONY: image.publish.backend
image.publish.backend:
	# ---------------------- PUBLISH BACKEND IMAGE
	${RUNNER} push ${BACKEND_IMAGE}

.PHONY: fmt
fmt:
	google-java-format -r ./src/main/java/dev/easbarba/apito/*/***
	google-java-format -r ./src/test/java/dev/easbarba/apito/*/***

.PHONY: system
system:
	guix shell --pure --container

.DEFAULT_GOAL := test
