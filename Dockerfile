# apito-jakartaee is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# apito-jakartaee is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with apito-jakartaee. If not, see <https://www.gnu.org/licenses/>.

ARG MAVEN_IMAGE=maven:3-eclipse-temurin-21
ARG OPENJDK_IMAGE=eclipse-temurin:21-jdk

FROM $MAVEN_IMAGE as builder
WORKDIR /app
COPY ./pom.xml .
RUN mvn dependency:go-offline

FROM $OPENJDK_IMAGE
MAINTAINER EAS Barbosa<easbarba@outlook.com>
WORKDIR /app
COPY ./src ./src
