-- Apito-Jakartaee is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Apito-Jakartaee is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Apito-Jakartaee. If not, see <https://www.gnu.org/licenses/>.

CREATE TABLE referees (id UUID PRIMARY KEY DEFAULT gen_random_uuid (), name varchar (50) NOT NULL, state varchar (19) NOT NULL, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, modified_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL);
