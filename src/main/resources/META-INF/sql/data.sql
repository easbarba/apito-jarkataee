-- Apito-Jakartaee is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Apito-Jakartaee is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Apito-Jakartaee. If not, see <https://www.gnu.org/licenses/>.

INSERT INTO referees (name, state) VALUES ('Pericles Bassols Pegado Cortez', 'Rio de Janeiro'), ('Raphael Claus', 'Sao Paulo'), ('Ricardo Marques Ribeiro', 'Minas Gerais'), ('Wilton Pereira Sampaio', 'Goias'), ('Anderson Daronco', 'Rio Grande do Sul'), ('Marcelo Aparecido de Souza', 'Sao Paulo'), ('Marcelo de Lima Henrique', 'Rio de Janeiro'), ('Sandro Meira Ricci', 'Minas Gerais'), ('Rafael Traci', 'Parana'), ('Dewson Fernando Freitas Silva', 'Parana');
