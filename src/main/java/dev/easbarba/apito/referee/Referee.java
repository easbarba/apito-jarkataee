/*
 *  Apito-Jakartaee is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Apito-Jakartaee is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Apito-Jakartaee. If not, see <https://www.gnu.org/licenses/>.
 */

package dev.easbarba.apito.referee;

import java.io.Serializable;
import java.util.UUID;

import dev.easbarba.apito.common.AbstractEntity;
import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;

@Entity
@Table(name = "referees")
@Cacheable
public class Referee extends AbstractEntity implements Serializable {
    @NotBlank(message = "Name may not be blank")
    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @NotBlank(message = "State may not be blank")
    @Column(name = "state", nullable = false, length = 60)
    private String state;

    public Referee() {
    }

    public Referee(UUID id, @NotBlank(message = "Name may not be blank") String name,
            @NotBlank(message = "State may not be blank") String state) {
        this.id = id;
        this.name = name;
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
