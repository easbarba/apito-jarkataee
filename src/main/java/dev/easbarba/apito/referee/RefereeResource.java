/*
 *  Apito-Jakartaee is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Apito-Jakartaee is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Apito-Jakartaee. If not, see <https://www.gnu.org/licenses/>.
 */

package dev.easbarba.apito.referee;

import java.util.List;
import java.util.UUID;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.PersistenceException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import dev.easbarba.apito.common.ResponseDTO;

@Path("/referees")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class RefereeResource {
  @Inject
  private RefereeServiceImpl refereeService;

  @GET
  @Path("/")
  public Response index() {
    return Response.ok(new ResponseDTO<List<RefereeDTO>>(refereeService.getAll()))
        .allow(HttpMethod.GET)
        .build();
  }

  @GET
  @Path("/{id}")
  public Response show(@PathParam("id") UUID id) {
    var data = refereeService.getOne(id);

    return Response.ok(new ResponseDTO<RefereeDTO>(data))
        .allow(HttpMethod.GET)
        .build();
  }

  @POST
  @Path("/")
  public Response create(Referee referee) {
    try {
      refereeService.save(referee);
      return Response.status(Response.Status.CREATED)
          .allow(HttpMethod.POST)
          .build();
    } catch (PersistenceException ex) {
      throw new WebApplicationException(Response.Status.BAD_REQUEST);
    }
  }

  @DELETE
  @Path("/{id}")
  public Response destroy(@PathParam("id") UUID id) {
    refereeService.remove(id);

    return Response.noContent()
        .allow(HttpMethod.DELETE)
        .build();
  }
}
