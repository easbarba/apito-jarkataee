/*
 *  Apito-Jakartaee is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Apito-Jakartaee is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Apito-Jakartaee. If not, see <https://www.gnu.org/licenses/>.
 */

package dev.easbarba.apito.referee;

import java.util.UUID;
import java.util.stream.Collectors;
import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

@ApplicationScoped
@Transactional
public class RefereeServiceImpl implements RefereeService {
  @Inject
  private RefereeDAO refereeDAO;

  @Inject
  private RefereeMapper refereeMapper;

  @Override
  public List<RefereeDTO> getAll() {
    return refereeDAO.findAll()
        .stream()
        .map(refereeMapper::toDTO)
        .collect(Collectors.toList());
  }

  @Override
  public RefereeDTO getOne(final UUID id) {
    var referee = refereeDAO.findById(id)
        .orElseThrow(() -> new WebApplicationException(Response.Status.NOT_FOUND));

    return refereeMapper.toDTO(referee);
  }

  @Override
  public RefereeDTO save(final Referee referee) {
    refereeDAO.persist(referee);

    return refereeMapper.toDTO(referee);
  }

  @Override
  public void remove(final UUID id) {
    refereeDAO.deleteById(id);
  }
}
