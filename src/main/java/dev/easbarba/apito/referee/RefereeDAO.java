/*
 *  Apito-Jakartaee is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Apito-Jakartaee is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Apito-Jakartaee. If not, see <https://www.gnu.org/licenses/>.
 */

package dev.easbarba.apito.referee;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Logger;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.persistence.PersistenceContext;

@Stateless
public class RefereeDAO {
  private final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

  @PersistenceContext(name = "apitoPUEclipseLink")
  protected EntityManager entityManager;

  public List<Referee> findAll() {
    logger.info("Getting all referees");
    return entityManager.createQuery("select r from Referee r", Referee.class)
        .getResultList();
  }

  public Optional<Referee> findById(UUID id) {
    logger.info("Getting Referee by id: " + id);

    var referee = entityManager.find(Referee.class, id);
    return Optional.ofNullable(referee);
  }

  @Transactional
  public Referee persist(Referee referee) {
    logger.info("Persisting referee by id: " + referee.getId());

    entityManager.persist(referee);
    entityManager.flush();

    return referee;
  }

  @Transactional
  public Referee update(Referee referee) {
    logger.info("Updating referee by id: " + referee.getId());

    return entityManager.merge(referee);
  }

  @Transactional
  public void deleteById(UUID id) {
    logger.info("Deleting referee by id: " + id);

    var referee = findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid referee by id: " + id));
    entityManager.remove(referee);
  }
}
