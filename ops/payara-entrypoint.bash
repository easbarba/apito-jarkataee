#!/usr/bin/env bash

asadmin \
    --user admin --passwordfile ./deployments/passwordfile  \
    create-jdbc-connection-pool \
        --datasourceclassname org.postgresql.ds.PGConnectionPoolDataSource \
        --restype javax.sql.ConnectionPoolDataSource \
        --property portNumber=$SQL_PORT:password=$SQL_PASSWORD:user=$SQL_USERNAME:serverName=$SQL_HOST:databaseName=$SQL_DATABASE
asadmin --user admin --passwordfile ./deployments/passwordfile \
        create-jdbc-resource \
            --connectionpoolid apito-pool jdbc/apito_ds
